import React from "react";
import { Task } from "./App";

interface TaskCardProps {
  task: Task;
}

const TaskCard: React.FC<TaskCardProps> = ({ task }) => {
  return (
    <div>
      <h4>{task.name}</h4>
    </div>
  );
};

export default TaskCard;
