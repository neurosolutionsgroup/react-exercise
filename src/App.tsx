import React, { useState } from "react";
import "./App.css";
import TaskCard from "./TaskCard";

export interface Task {
  name: string;
}

function App() {
  const [text, setText] = useState("");
  const [tasks, setTasks] = useState<Task[]>([]);

  const onTextChange = (value: string) => {};

  const onSubmit = () => {
    setText("");
  };

  return (
    <div className="App">
      <h1>React Interview Exercise</h1>

      <h2>To Do: </h2>

      <div>
        <label htmlFor="text-input">Task:</label>
        <input
          id="text-input"
          value={text}
          onChange={(e) => onTextChange(e.currentTarget.value)}
        />
        <button onClick={onSubmit}>Submit</button>
      </div>
      <hr />
      <div>
        <h3>Tasks:</h3>
      </div>
    </div>
  );
}

export default App;
