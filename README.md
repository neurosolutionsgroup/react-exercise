# React Interview Exercise

## Getting Started

To use the app in development you need to install the npm dependencies.
When you've done this you can test the app via the `start` script.

## Steps

The basic structure has been setup for a to-do list. This is what is left to do:

- Finish setting up state for the text input
- Set state of list of tasks when submitting a new task
- Display list of tasks using the `TaskCard` component
- Add numbering of tasks to task cards
- Add ability to mark each task as "done" with a checkbox
- Update styling of the task when task is "done"
  - Styling of your choice, e.g. strikethough on text, text color, background color
- Bonus: Add a checkbox that lets the user mark all the tasks "done" at once
